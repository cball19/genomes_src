#!/usr/bin/env python
ex=\
"""
Read a RAP-DB GFF files and convert to BED-detail.
Reads from stdin or one or more files.
Writes to stdout.

example)

  %prog > RAPDB.bed
  
"""

import sys,optparse,re
import Mapping.FeatureModel as feature
import Mapping.Parser.Bed as Bed

tab='\t'
nl='\n'

def main(args):
    fh = open('transcripts.gff')
    subfeats={}
    mRNAs={}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks=line.rstrip().split(tab)
        if toks[2]=='CDS':
            parseGffLine(line=line,mRNAs=mRNAs,subfeats=subfeats)
    fh = open('transcripts_exon.gff')
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        parseGffLine(line=line,mRNAs=mRNAs,subfeats=subfeats)
    for parent_ID in subfeats.keys():
        mRNA = mRNAs[parent_ID]
        for sub_feat in subfeats[parent_ID]:
            mRNA.addFeat(sub_feat)
    Bed.feats2bed(mRNAs.values(),bed_format='BED14')

def parseGffLine(line=None,mRNAs=None,subfeats=None):
    (seqname,producer,feat_type,start,end,
     ignore1,strand,ignore2,extra_feat)=line.strip().split('\t')
    if not feat_type in ['mRNA','CDS','exon']:
        return None
    start=int(start)-1
    end=int(end)
    length=end-start
    if strand=='+':
        strand=1
    elif strand=='-':
        strand=-1
    seqname=re.sub('r0','r',seqname)
    if seqname.startswith('c'):
        seqname='C'+seqname[1:]
    key_vals=parseKeyVals(extra_feat)
    if feat_type == 'mRNA':
        mRNA_id=key_vals['Name']
        key_vals['gene_name']=key_vals['Locus_id']
        feat=feature.CompoundDNASeqFeature(display_id=mRNA_id,
                                           feat_type='mRNA',
                                           key_vals=key_vals,
                                           seqname=seqname,
                                           start=start,
                                           length=length,
                                           strand=strand)
        mRNAs[mRNA_id]=feat
    else:
        mRNA_id=key_vals['Parent']
        feat = feature.DNASeqFeature(seqname=seqname,
                                     start=start,
                                     length=length,
                                     strand=strand,
                                     key_vals=key_vals,
                                     feat_type=feat_type)
        if subfeats.has_key(mRNA_id):
            subfeats[mRNA_id].append(feat)
        else:
            subfeats[mRNA_id]=[feat]


def parseKeyVals(extra_feat):
    "Parse extra feature fields."
    if extra_feat.endswith(';'):
        vals = extra_feat.split(';')[:-1]
    else:
        vals = extra_feat.split(';')
    key_vals = {}
    for item in vals:
        pair=item.split('=')
        if not len(pair)==2:
            raise ValueError("GFF line has weird extra feature value: %s."%extra_feat)
        key = pair[0]
        val = pair[1]
        if key_vals.has_key(key):
            raise ValueError("GFF line has two values for same key in extra feat field: %s."%extra_feat) 
        key_vals[pair[0]]=pair[1]
    return key_vals
            
if __name__ == '__main__':
    usage='%prog\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)
