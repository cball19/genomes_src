#!/usr/bin/env python

"""Classes for modeling features on DNA sequence and other biological molecules."""

import re,sys

class KeyVal:
    """An object that has key-value pairs."""

    def __init__(self,key_vals=None,**kwargs):
        self.setKeyVals(key_vals)

    def setKeyVals(self,key_vals):
        """
        Function: set a key and value pairs dictionary
        Returns :
        Args    : key_vals, a dictionary
        """
        self._dict = key_vals

    def setKeyVal(self,key,val):
        """
        Function: set a key and value pair
        Returns :
        Args    : two entities, key must be hashable
        """
        try:
            self._dict[key]=val
        except TypeError:
            if not self._dict:
                self._dict = {}
                self._dict[key]=val

    def getKeyVals(self):
        """
        Function: get the dictionary of key/value pairs
        Returns : a dictionary, or None if set_key_vals has not
                  be called
        Args    :
        """
        return self._dict

    def getVal(self,key):
        """
        Function: Get the value associated with the given key
        Returns : the value, or None if not set
        Args    : a hashable value, the key
        """
        try:
            return self.getKeyVals()[key]
        except Exception:
            return None

class Range:
    """Represents a range on a sequence. Uses interbase coordinate system."""

    def __init__(self,start=None,length=None,**kwargs):
        """
        Function: make a new Range
        Returns : a new Range
        Args    : start - non-negative int, start position, first residues is numbered 0
                  length - non-negative int, length of the range (number of residues)
        """
        self.setStart(start)
        self.setLength(length)

    
    def getStart(self):
        """
        Function: gets start of this range
        Returns :  non-negative int or None
        Args    :
        """
        return self._start

    def setStart(self,start):
        """
        Function: sets the start of this range
        Returns :
        Args    : non-negative int
        """
        m = "Start must be a non-negative int or None"
        self.checkType(start,0,m)
        self.checkCoord(start,m)
        self._start = start

    def getLength(self):
        """
        Function: Gets the length of this range
        Returns : non-negative int, the number of bases this Range encompasses
        Args    :
        """
        return self._length

    def setLength(self,length):
        """
        Function: Sets the length of this range
        Returns : the length of this range
        Args    : length - integer
        """
        m = "Length must be a non-negative int or None"
        self.checkType(length,0,m)
        self.checkCoord(length,m)
        self._length = length

    def getEnd(self):
        if not self._length == None and not self._start == None:
            return self._start + self._length
        else:
            return None
    """
    def setEnd(self, end):
        if self._start:
            self.setLength(end - self._start)
        else:
            raise ValueError("Can't set end without a start")
    """

    def overlaps(self,r):
        """
        Function: tests if r2 overlaps r1
        Args    : r - another Range
        Returns : True or False

        Overlap means: positions (residues) in common.

        e.g.,

    overlapping:

        s1        s1 + len1
         |-----------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
               |-----------|
              s2        s2 + len2

              s1        s1 + len1
               |-----------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
               |-----------|
              s2        s2 + len2

    not overlapping:

        s1        s1+len1
         |-----------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
                     |-----------|
                    s2        s2 + len2

        """
        s1 = self.getStart()
        e1 = s1 + self.getLength()
        s2 = r.getStart()
        e2 = s2 + r.getLength()
        return (min(e1,e2)-max(s1,s2))>0

    def isLessThan(self,r):
        """
        Function: compares the location of this to r
        Args    : r - a Range
        Returns : True if this doesn't overlap r and is
                  located upstream (to the left) of r 
        """
        myseq = self.getSeqName()
        otherseq = r.getSeqName()
        if not myseq==otherseq:
            return myseq < otherseq
        elif not self.overlaps(r):
            return self.getEnd()<r.getStart()
        else:
            return False

                    
    def contains(self,r):
        """
        Function: tests if r1 contains r2
        Args    : arg #1 = a range to compare this one to (mandatory)
        Returns : true if this contains r2, false if not

    contained:

        s1                     s1 + len1
         |------------------------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
               |-----------|
              s2        s2 + len2

    not contained:

        s1               s1+len1
         |-----------------|
          nnnnn nnnnn nnnnn nnnnn nnnnn
                     |-----------|
                    s2        s2 + len2

        """
        s1 = self.getStart()
        len1 = self.getLength()
        s2 = r.getStart()
        len2 = r.getLength()
        return s2 >= s1 and s2 + len2 <= s1 + len1

    def equals(self,r):
        """
        Function: tests if r2 equals r1 (they have the same coordinates)
        Args    : r, another Range
        Returns : True if start, length are equal, False if not
        """
        s1 = self.getStart()
        len1 = self.getLength()
        s2 = r.getStart()
        len2 = r.getLength()
        return (s1 == s2 and len1 == len2)

    def checkType(self,val,exemplar,message):
        """
        Function: Check that val, exemplar are the same Type, raise a new TypeError
                  with the given message if not
                  If val is None, don't bother.
        Args    : val, exemplar - two objects, or None
                  message - string, argument to TypeError constructor
        Returns :
        """
        if val is not None and not type(val)==type(exemplar):
                raise TypeError(message)

    def checkCoord(self,val,message):
        """
        Function: Check val is non-negative. If not, raise a ValueError using the
                  given message.
        Args    : val - a number
                  message - string, argument to ValueError constructor
        Returns :
        """
        if val and val < 0:
            raise ValueError("%s:%s"%(message,str(val)))


class SeqFeature(Range,KeyVal):
    """A feature on a sequence, such as protein, DNA, or RNA"""

    def __init__(self,display_id=None,producer=None, seqname=None, feat_type=None,**kwargs):
        Range.__init__(self,**kwargs)
        KeyVal.__init__(self,**kwargs)
        self.setSeqname(seqname)
        self.setDisplayId(display_id)
        self.setFeatType(feat_type)
        self.setProducer(producer)

    def getDisplayId(self):
        """
        Function: get the display_id, a string
        Returns : the display_id, such as Genbank Accession
        Args    :

        If the display_id has not been set, returns None.
        """
        return self._display_id

    def setDisplayId(self, display_id):
        """
        Function: set the display_id, a string
        Returns : the display_id, such as Genbank Accession
        Args    : display_id - string
        """
        self._display_id = display_id

    def setSeqname(self,seqname):
        """
        Function: set name of annotated sequence
        Args    : a string, or None
        Returns :
        """
        self._seqname = seqname

    def getSeqname(self):
        """
        Function: get name of annotated sequence
        Args    :
        Returns : the name of the annotated sequence or None if not set
        """
        return self._seqname

    def setProducer(self,producer):
        """
        Function: set the producer of the annotation
        Args    : a string, or None
        Returns :

        For example, producer corresponds to the producer (2nd) field
        in a GFF format file.
        """
        self._producer= producer

    def getProducer(self):
        """
        Function: get the producer of the annotation
        Args    :
        Returns : string, the name of the producer or None if not set

        For example, producer corresponds to the producer (2nd) field
        in a GFF format file.
        """
        return self._producer

    def setFeatType(self,feat_type):
        """
        Function: set feature type
        Args    : a string
        Returns :

        Set the feat_type of SeqFeature this is.
        The feat_type must be listed in feat_types dictionary.

        For example, feat_type corresponds to the feature (3rd) field in a
        GFF format file.
        """
        self._feat_type = feat_type

    def getFeatType(self):
        """
        Function: get feature type
        Args    :
        Returns : string, the type of feature this is, or None if not set

        For example, type corresponds to the feature (3rd) field in a
        GFF format file.
        """
        return self._feat_type


class DNASeqFeature(SeqFeature):
    """A feature on a DNA sequence."""

    def __init__(self,strand=None, score=None, **kwargs):
        SeqFeature.__init__(self,**kwargs)
        self.setStrand(strand)
        self.setScore(score)

    def getStrand(self):
        """
        Function: get the strand of this range
        Returns : the strand of this range, -1, 1 or None
        Args    :
        """
        return self._strand

    def setStrand(self,strand):
        """
        Function: set the strand of this range
        Returns :
        Args    : strand - 1, -1, or None
        """
        if strand in [1,-1,None]:
            self._strand = strand
        else:
            raise ValueError("DNASeqFeature setStrand requires 1, -1, or None")

    def getScore(self):
        """
        Function: get the score of this sequence
        Returns : the score of this sequence
        Args    :
        """
        return self._score

    def setScore(self,newScore):
        """
        Function: set the score of this sequence
        Returns :
        Args    : score (None or int)
        """
        if newScore is None or isinstance(newScore, int):
            self._score = newScore

    def contains(self,feat):
        """
        Function: tests if this contains another DNASeqfeature and
                  both are on the same strand
        Args    : feat - another DNASeqFeature
        Returns : true if this contains the other, false if not
        """
        return self.getStrand() == feat.getStrand() and Range.contains(self,feat)

    def equals(self,feat):
        """
        Function: tests if this has the same coordinates and strand as
                  another DNASeqFeature
        Args    : arg #1 = a DNASeqFeature to compare this one to (mandatory)
                  r - boolean: if True, ignore Strand
        Returns : true if the coordinates and strand are equal, false if not
        """
        return self.getStrand() == feat.getStrand() and Range.equals(self,feat)

    def overlaps(self, r, ignore_strand = False):
        """
        Function: tests if r2 overlaps r1
        Args    : r - another DNASeqFeature
        Returns : True or False
        """
        if not ignore_strand:
            strandsMatch = (self.getStrand() == r.getStrand())
        else:
            strandsMatch = True

        return (Range.overlaps(self,r) and
	        self.getSeqname() == r.getSeqname() and
	        strandsMatch)

    def isFivePrimeOf(self,other):
        """
        Function: Determine whether we are located 5-prime the other DNASeqFeature,
                  relative to its start of transcription.
                  If the other's strand is not set, then return True if we are located
                  upstream. Also see isThreePrimeOf
        Returns : True or False
        Args    : other, a DNASeqFeature
        """
        strand = other.getStrand()
        if not strand:
            strand = 1
        my_start = self.getStart()

        other_start = other.getStart()
        if strand == 1:
            if my_start < other_start:
                return True
        else:
            my_stop = self.getStart() + self.getLength()
            other_stop = other.getLength() + other_start
            if my_stop > other_stop:
                return True
        return False


    def isThreePrimeOf(self,other):
        """
        Function: determine whether this is three prime of another
                  DNASeqFeature
        Returns : True or False
        Args    : a DNASeqFeature or a Stranded Range

        Determine whether this DNASeqFeature contains positions
        (bases) located at positions 3-prime of (left of, if +1
        strand, right of if -1 strand) of every position (bases)
        in the given DNASeqFeature.

        The two must be on the same strand to be compared in this
        way.

        Here's an example:

        xxxxxx
                yyyyyyyyyy
        5'                               3'
        ---------------------------------- +1 strand
        ---------------------------------- -1 strand
        3'                               5'
                   aaaaaaaaaaaa
                                 bbbbbbb

        y.is_three_prime_of(x) returns: True
        a.is_three_prime_of(b) returns: True
        x.is_three_prime_of(a) returns: An error!  They are not
        on the same strand.

        """
        strand = self.getStrand()
        if not strand:
            strand = 1
        my_start = self.getStart()
        other_start = other.getStart()
        if strand == 1:
            my_stop = self.getStart() + self.getLength()
            other_stop = other.getLength() + other_start
            if my_stop > other_stop:
                return True
        else:
            if my_start > other_start:
                return True
        return False

    def overlaps(self,other,ignore_strand=True):
        """
        Function: find out if this overlaps the other
        Returns : True or False
        Args    : a DNASeqFeature if ignore_strand is False
                  a Range otherwise
        """
        if ignore_strand or self.getStrand()==other.getStrand():
            # if ignore_strand was true, then we don't execute getStrand
            # if other doesn't have a getStrand method (e.g., not a DNASeqFeature or better)
            # then there will be an error, which is good because it will signal a problem
            # in the user's code - we want that 
            return SeqFeature.overlaps(self,other)
        else:
            return False
            
            
class CompoundDNASeqFeature(DNASeqFeature):
    """A SeqFeature that contains other SeqFeatures."""

    def __init__(self, feats=[], start=None, length=None, **kwargs):
        DNASeqFeature.__init__(self, start=start, length=length, **kwargs)
        self._feats_list = []  # a list, contains all features
        for feat in feats:
            self.addFeat(feat,1)
        self._sorted=False # whether or not the feature_list is sorted

    def getFeats(self,feat_type=None):
        """
        Function: get subfeats of certain type, if given
        Returns : all the feats or just feats of the given feat_type
        Args    : nothing, or a feat_type

        Return a new list containing the requested subfeats
        in whatever order they are stored internally.

        If no subfeatures of the requested have been added, return an empty list.
        """
        if feat_type:
            lst = filter(lambda x:x.getFeatType()==feat_type,self._feats_list)
        else:
            # a hack, want to return a new list, not reference to internal data structure
            lst = filter(lambda x:True,self._feats_list) 
        return lst

    def addFeat(self,feat,safe=0):
        """
        Function: add a SeqFeature to this CompoundDNASeqFeature
        Returns :
        Args    : a SeqFeature or derived class
                  safety flag (default 0)

        Add a SeqFeature to the list.  If the safe flag is non-zero,
        do not check whether the feature has already been added.
        """
        if safe:
            self._feats_list.append(feat)
            self._sorted=False
        else:
            if feat not in self._feats_list:
                self._feats_list.append(feat)
                self._sorted=False

    def removeFeat(self,feat):
        """
        Function: remove a SeqFeature from this CompoundDNASeqFeature
        Returns :
        Args    : a SeqFeature or derived class
        """
        self._feats_list.remove(feat)
        
    def getSortedFeats(self,feat_type=None):
        """
        Function:
        Returns : a list of all the feats or just feats of the given feat_type
                  sorted by start position
        Args    : nothing, or a feat_type

        Return a list containing the requested subfeats
        sorted (low to high) by start position.
        """
        return self.sortedFeats(feat_type=feat_type)

    def sortedFeats(self,feat_type=None):
        """
        Function:
        Returns : a list of all the feats or just feats of the given feat_type
                  sorted by start position
        Args    : nothing, or a feat_type

        Return a list containing the requested subfeats
        sorted (low to high) by start position.
        """
        if not self._sorted:
            #self._feats_list.sort(lambda x,y:x.getStart()-y.getStart())
            #self._feats_list=sorted(self._feats_list,key=lambda x:x.getStart())
            self._feats_list.sort(key=lambda x:x.getStart())
            self._sorted=True
        return self.getFeats(feat_type=feat_type)

    def useSubFeatCoords(self):
        """
        Function: Update start, length, strand using subfeatures
        Returns : 
        Args    : 
        """
        if len(self._feats_list)>0:
            feats=self.getSortedFeats()
            self.setStart(feats[0].getStart())
            # find longest end
            ends=map(lambda x:x.getEnd(),feats)
            end=max(ends)
            self.setLength(end-self.getStart())
            self.setStrand(feats[0].getStrand())

    def getTranslStartAndEnd(self):
        cdss = self.getSortedFeats(feat_type="CDS")
        if len(cdss)>0:
            thick_start = cdss[0].getStart()
            thick_end = cdss[-1].getStart()+cdss[-1].getLength()
        else:
            thick_start = self.getStart()
            thick_end = thick_start
        return (thick_start, thick_end)
