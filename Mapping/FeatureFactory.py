#!/usr/bin/env python

"""Functions useful for creating DNASeqFeature objects"""

import sys,os
import Mapping.FeatureModel as F

def make_feat_from_gff_fields(vals=None,cfeat=False):
    "For backward compatibility"
    return makeFeatFromGffFields(vals=vals,cfeat=cfeat)

def makeFeatFromGffFields(vals=None,cfeat=False):
    """
    Function: Create a DNASeqFeature using the given list of fields from
              a GFF line.
    Returns :
    Args    : vals - a list of strings, the first 7 fields in a GFF record
              cfeat - if True, make a CompoundDNASeqFeature
                      if False, make DNASeqFeature

    Corrects for one-based. Don't do this in advance. Container feat will be
    whatever was in the feature type field. Frame is currently ignored.
    """
    seqname = vals[0]
    producer=vals[1]
    typ = vals[2]
    start = int(vals[3])-1
    end = int(vals[4])
    length = end-start
    strand = vals[6]
    if strand == '+':
        strand = 1
    elif strand == '-':
        strand = -1
    elif strand == '.':
        strand = 0
    else:
        raise ValueError("Unrecognized strand in GFF")
    if cfeat:
        f = F.CompoundDNASeqFeature(seqname=seqname,
                                    producer=producer,
                                    feat_type=typ,
                                    start=start,
                                    length=length,
                                    strand=strand)
    else:
        f = F.DNASeqFeature(seqname=seqname,
                            producer=producer,
                            feat_type=typ,
                            start=start,
                            length=length,
                            strand=strand)
    return f

def cfeat_from_feats(subfeats,display_id=None,
                     group_feat_type='transcript',producer=None,
                     boundary_feat='exon'):
    """
    Function: use the given list of features to assemble a container
              CompoundDNASeqFeature
    Returns : CompoundDNASeqFeature
    Args    : subfeats - list of subfeatures, e.g., DNASeqFeature objects representing exons
              display_id - the name of the container feature 
              group_feat_type - the type for the container feature (typically, mRNA or transcript)
              boundary_feat - the subfeature type that delimits the
                              container feature's 5- and 3-prime
                              boundaries

    Used by Utils.Igb
    """
    strand = subfeats[0].strand()
    seqname = subfeats[0].seqname()
    if not producer:
        producer=subfeats[0].producer() # this can be None and not cause a problem
    for subfeat in subfeats:
        if strand != subfeat.strand() or \
               seqname != subfeat.seqname() or \
               producer != subfeat.producer() :
            raise ValueError("Warning: Subfeat strand, seqname, or producer do not match: " + display_id)

    cfeat = feature.CompoundDNASeqFeature(seqname=seqname,
                                          producer=producer,
                                          type=group_feat_type,
                                          feats=subfeats,
                                          strand=strand)
    if display_id:
        cfeat.setDisplayId(display_id)
    sorted_feats = cfeat.sorted_feats(type=boundary_feat)
    first_feat = sorted_feats[0]
    start=first_feat.start()
    cfeat.start(start)
    end = start + first_feat.length()
    for subfeat in sorted_feats:
        candidate_end = subfeat.start() + subfeat.length()
        if candidate_end > end:
            end = candidate_end
    length=end-start
    cfeat.length(length)
    return cfeat

def mergeExons(cfeat):
    """
    Function: Check the exons subfeatures. If any overlap, merge them.
    Returns :
    Args    : cfeat - a CompoundDNASeqFeature object with exon subfeatures

    Use this when working with GFF files that use three_prime_UTR, five_prime_UTR,
    and CDS features to define exon boundaries instead of exon features.

    For an example, see Mapping.Parser.ParvulaGff.
    """
    exons = cfeat.getSortedFeats('exon')
    if len(exons)==0:
        raise ValueError("Feature %s has no exon subfeatures"%cfeat.getDisplayId())
    i = 0
    N = len(exons)
    while i < N-1:
        this_exon = exons[i]
        next_exon = exons[i+1]
        if this_exon.getEnd()>=next_exon.getStart():
            new_length=next_exon.getLength()+this_exon.getLength()
            next_exon.setStart(this_exon.getStart())
            next_exon.setLength(new_length)
            cfeat.removeFeat(this_exon)
        i = i + 1
    return cfeat

    

