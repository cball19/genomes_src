"""Functions for reading and writing UCSC 'bed' files."""

import os,sys,string

import Mapping.FeatureModel as feature
import Utils.General as utils

sep1 = '\t'
sep2 = ','

def bed2feats(fname=None,producer=None,has_orf=1,format='ucsc',
              group_feat='transcript',sub_feat='exon',skip_uns=0, reset_scores = False):
    """
    Function: read features from bed format file
    Returns : a list of CompoundDNASeqFeature objects
    Args    : fname - name of file to read
              producer - producer of the annotations, the track name
              has_orf - whether or not the features should have ORFs
              format - if ucsc, then block_starts are relative to the
                       start of the feature, not the chromosome

    Ex) feats = bed2feats(fn='mm7.mRNA.bed.gz',producer='mm7.mRNA',
                          has_orf=1,format='ucsc')

    """
    fh = utils.readfile(fname)
    feats = []
    for line in fh:
        if line.startswith('track'):
            continue
        feat = bedline2feat(line,producer=producer,has_orf=has_orf,
                            format=format,group_feat=group_feat,
                            sub_feat=sub_feat,skip_uns=skip_uns, reset_scores = reset_scores)
        if feat:
            feats.append(feat)
    fh.close()
    return feats

def bedline2feat(line,group_feat='mRNA',sub_feat='exon',
                 producer=None,has_orf=1,format='ucsc',skip_uns=0, reset_scores = False):
    """
    Function: convert the given bed format line to a
              CompoundDNASeqFeature object
    Example : feat = bedline2feat(line,group_feat='transcript',
                                  sub_feat='exon',producer='mm7.mRNA',
                                  has_orf=1,format='ucsc',skip_uns=1)
    Returns : a CompoundDNASeqFeature object
    Args    : group_feat - the parent feat type
              sub_feat - the subfeature type
              producer - producer of the annotations, the track name
              has_orf - whether or not the features should have ORFs
              format - if ucsc, then block_starts are relative to the
                       start of the feature, not the chromosome
              skip_uns - if the annotated sequence chrUn or random,
                         skip it

    Note that we handle CDS features (thickStart and thickEnd)
    coordinates as follows: UCSC BED files' thickStart and thickEnd denote
    the start and end positions of the first and last translated regions.
    For forward strand gene models, the thickEnd indicates the end (in
    positive-strand interbase) of the stop codon, which is NOT the last
    translated codon, and the thickStart indicates the position of the first
    base of the first translated codon, e.g., the A in the ATG methionine
    start codon. For minus strand genes, the thickEnd bounds the start codon,
    reverse complemented as 5-CAT-3 and the thickStart indicates the last
    base of the stop codon, e.g., the first T in the reverse-complemented
    5-TTA-3.
    """
    vals=line.rstrip().split('\t')
    try:
        [seqname,start,end,name,score,strand,
         thick_start,thick_end,na,blockcount,
         blocklens,blockstarts]=vals[0:12]
    except ValueError, err:
        raise ValueError("ValueError %s on line: %s."%(str(err),line.rstrip()))
    if skip_uns:
        if seqname == 'chrUn' or seqname.endswith('random'):
            return None
    start = int(start)
    end = int(end)

    if reset_scores:
        score = 0
    else:
        score = int(score)
        
    length = end-start
    if strand == '+':
        strand = 1
    elif strand == '-':
        strand = -1
    parent_feat = feature.CompoundDNASeqFeature(display_id=name,
                                                start=start,
                                                length=length,
                                                feat_type=group_feat,
                                                seqname=seqname,
                                                strand=strand,
                                                score = score,
                                                producer=producer)
    if has_orf:
        orf_start = int(thick_start)
        orf_length = int(thick_end)-orf_start
        orf = feature.DNASeqFeature(start=orf_start,
                                    length=orf_length,
                                    strand=strand,
                                    seqname=seqname,
                                    feat_type='CDS',
                                    producer=producer,
                                    display_id=id)
        parent_feat.addFeat(orf)
    bl_starts = map(lambda x:int(x),blockstarts.rstrip(sep2).split(sep2))
    if format == 'ucsc':
        bl_starts = map(lambda x:x+start,bl_starts)
    bl_lens = map(lambda x:int(x),blocklens.rstrip(sep2).split(sep2))
    blockcount = int(blockcount)
    i = 0
    while i < blockcount:
        subfeat = feature.DNASeqFeature(start=bl_starts[i],
                                        length=bl_lens[i],
                                        strand=strand,
                                        seqname=seqname,
                                        producer=producer,
                                        display_id=id,
                                        feat_type=sub_feat)
        parent_feat.addFeat(subfeat)
        i += 1
    if len(vals)==14: # bed detail
        sym = vals[12]
        descr=vals[13]
        parent_feat.setKeyVal('symbol',sym)
        parent_feat.setKeyVal('descr',descr)
    return parent_feat

def single_span_feat2bed(feat):
    """
    Function: Form and return a bed format line representing the
              given feature
    Returns : a line (newline-terminated)
    Args    : feat - a DNASeqFeature object no sub features

    thickStop will be same as thickStart.
    """
    rgb = '0'
    score = '0'
    if feat.getVal('rgb'):
        rgb=feat.getVal('rgb')
    if feat.getScore():
        score = str(feat.getScore())
    start = feat.getStart()
    end = feat.getStart()+feat.getLength()
    if feat.getStrand()==1:
        strand = '+'
    elif feat.getStrand()==-1:
        strand = '-'
    else:
        strand = '.'
    seqname = feat.getSeqname()
    name = feat.getDisplayId()
#    blockstarts = '%i,'%start
    blockstarts = '0,'
    blocklens= '%i,'%feat.getLength()
    blockcount='1'
    thick_start = repr(start)
    thick_end = thick_start
    start = repr(start)
    end = repr(end)
    line = sep1.join([seqname,start,end,name,score,strand,
                      thick_start,thick_end,rgb,blockcount,
                      blocklens,blockstarts])+os.linesep
    return line
    
def feat2bed(feat,bed_format='BED12'):
    """
    Function: Form and return a bed format line representing the
              given feature
    Returns : a line (newline-terminated)
    Args    : feat - a CompoundDNASeqFeature object with zero or more
                     subfeatures of type 'exon'

    If translated, the feat must have sub-features of type CDS.
    thickStop encompasses the stop codon.

    If not translated, thickStop should equal thickStart.
    """
    rgb = '0'
    score = '0'
    if feat.getVal('rgb'):
        rgb=feat.getVal('rgb')
    if feat.getScore():
        score = str(feat.getScore())
    seqname = feat.getSeqname()
    start = feat.getStart()
    cdss = feat.getSortedFeats(feat_type="CDS")
    name = feat.getDisplayId()
    if cdss and len(cdss)>0:
        thick_start = cdss[0].getStart()
        thick_end = cdss[-1].getStart()+cdss[-1].getLength()
    else:
        thick_start = start
        thick_end = thick_start
    thick_start = repr(thick_start)
    thick_end = repr(thick_end)
    end = repr(feat.getStart()+feat.getLength())
    start = repr(start)
    if feat.getStrand()==1:
        strand = '+'
    elif feat.getStrand()==-1:
        strand = '-'
    else:
        strand = '.'
    name = feat.getDisplayId()
    exons = feat.getSortedFeats(feat_type='exon')
    if not exons:
        exons = [feat]
        sys.stderr.write("Warning: " + feat.getDisplayId() + " has no exons." + os.linesep)
    blockstarts = sep2.join(map(lambda x:repr(x.getStart()-feat.getStart()),exons)) + sep2
    blocklens = sep2.join(map(lambda x:repr(x.getLength()),exons))+sep2
    blockcount = repr(len(exons))
    line = sep1.join([seqname,start,end,name,score,strand,
                      thick_start,thick_end,rgb,blockcount,
                      blocklens,blockstarts])
    if bed_format=='BED14':
        note=feat.getVal('Note')
        if not note:
            note='NA'
        gene_name = feat.getVal('gene_name')
        if not gene_name:
            gene_name='NA'
        line = sep1.join([line,gene_name,note])
    line = line + '\n'
    return line

def feats2bed(feats,fname=None,tname=None,rgb=False,bed_format='BED12'):
    """
    Function: write a feature object to BED12 or BED14 format
    Returns :
    Args    : feats - a dictionary or list of feature objects keyed by the
                    feature display_id, it will become field four
              fname - the ngame of the file to write [required]
              tname - the track name [optional]
              bed_format - BED12 or BED14 (bed detail) [default is BED12]

    If tname not provided, don't write a track line.
    If using BED14, feature should have key value pair gene_name (field 13) and
    note (field 14). If these data are not available, NA (for not available) will
    be inserted in these two fields.
    """
    if not feats:
        sys.stderr.write("No features to write."+os.linesep)
        return
    if utils.is_dict(feats):
        lst = feats.values()
    else:
        lst = feats
    if not fname:
        fh = sys.stdout
    else:
        fh = open(fname,'w')
    if tname:
        if rgb:
            fh.write('track name="%s" itemRgb="on"%s'%(tname,os.linesep))
        else:
            fh.write('track name="%s"%s'%(tname,os.linesep))
    for feat in lst:
        line = feat2bed(feat,bed_format=bed_format)
        fh.write(line)
    if fname:
        fh.close()

        
