"""Functions for reading Phytozome GFF3 files."""

import Mapping.FeatureModel as feature
import Utils.General as utils
import sys

"""
This contains functions for parsing CuffLinks GTF, which reports transcript and exon features.
"""

def gff2feats(fname=None):
    """
    Function: read features from Cufflinks GTF format file
    Returns : list of CompoundDNASeqFeature objects representing spliced
              mRNA transcripts
    Args    : fname - name of file to read
    """
    lineNumber = 0
    fh = utils.readfile(fname)
    mRNAs = {}
    exons={}
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        try:
            (seqname,producer,feat_type,start,end,
             ignore1,strand,ignore2,extra_feat)=line.strip().split('\t')
        except ValueError:
            sys.stderr.write("Exception occurred at line %i:\n%s" % (lineNumber,line))
            raise
        if feat_type in ['transcript','exon']:
            start = int(start)-1
            end = int(end)
            length=end-start
            if strand == '+':
                strand = 1
            elif strand == '-':
                strand = -1
            elif strand == '.':
                strand = None
            key_vals = parseKeyVals(extra_feat)
            display_id=key_vals['transcript_id']
            if feat_type=='transcript':
                feat = feature.CompoundDNASeqFeature(seqname=seqname,
                                                     display_id=display_id,
                                                     start=start,
                                                     length=length,
                                                     strand=strand,
                                                     key_vals=key_vals,
                                                     feat_type='mRNA')
                if mRNAs.has_key(display_id):
                    raise ValueError("Two transcripts have the same transcript_id: %s"%ID)
                mRNAs[display_id]=feat
            else:
                feat = feature.DNASeqFeature(seqname=seqname,
                                             start=start,
                                             length=length,
                                             strand=strand,
                                             key_vals=key_vals,
                                             feat_type=feat_type)
                if not exons.has_key(display_id):
                    exons[display_id]=[]
                exons[display_id].append(feat)
    for display_id in exons.keys():
        exon_list = exons[display_id]
        mRNA=mRNAs[display_id]
        for exon in exon_list:
            mRNA.addFeat(exon)
    return mRNAs.values()

def parseKeyVals(extra_feat):
    """
    Parse extra feature fields. Cufflinks has format like the following example:
    
    gene_id "CUFF.9"; transcript_id "CUFF.9.1"; exon_number "6"; FPKM "7.5759518833"; frac "1.000000"; conf_lo "7.369160"; conf_hi "7.782743"; cov "169.175398";
    """
    if not extra_feat.endswith(';'):
        raise ValueError("CuffLinks format changed.")
    extra_feat=extra_feat[0:-1]
    vals = extra_feat.split('; ')
    key_vals = {}
    for item in vals:
        pair=item.split(' ')
        if not len(pair)==2:
            raise ValueError("GTF line has weird extra feature value: %s."%extra_feat)
        key = pair[0]
        val = pair[1]
        if not val.startswith('"') or not val.endswith('"'):
            raise ValueError("GTF value not enclosed in quotes: %s."%val)
        val=pair[1][1:-1]
        if key_vals.has_key(key):
            raise ValueError("GFF line has two values for same key in extra feat field: %s."%extra_feat) 
        key_vals[key]=val
    return key_vals
            
        
