"""Functions for reading Phytozome GFF3 files."""

import Mapping.FeatureModel as feature
import Utils.General as utils
import sys

"""
This contains functions for parsing Phytozome's variant of GFF3, which organizes gene models as follows:

Phytozome GFF files named *_gene_exon.gff3 report the following feature types for each gene model:

CDS
exon
five_prime_UTR
gene
mRNA
three_prime_UTR

How it handles non-coding gene models, I have no idea.

To construct a gene model from this, we have to build exon and CDS subfeatures.

We can ignore five_prime_UTR and three_prime_UTR features.
"""
def gff2feats(fname=None):
    """
    Function: read features from Phytozome GFF3 format file
    Returns : list of CompoundDNASeqFeature objects representing spliced
              mRNA transcripts
    Args    : fname - name of file to read
    """
    lineNumber = 0
    fh = utils.readfile(fname)
    mRNAs = {}
    # first get the mRNAs
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        if len(line)<9:
            # to avoid blank lines
            sys.stderr.write("Warning: weird line %i:%s\n"%(lineNumber,line))
            continue
        try:
            (seqname,producer,feat_type,start,end,
             ignore1,strand,ignore2,extra_feat)=line.strip().split('\t')
        except ValueError:
            sys.stderr.write("Exception occurred at line %i:\n%s" % (lineNumber,line))
            raise
        if feat_type == 'mRNA':
            start = int(start)-1
            end = int(end)
            length=end-start
            if strand == '+':
                strand = 1
            elif strand == '-':
                strand = -1
            else:
                raise ValueError("Strand makes no sense for: %s"%line)
            key_vals = parseKeyVals(extra_feat)
            display_id=key_vals['Name']
            ID=key_vals['ID']
            if mRNAs.has_key(ID):
                raise ValueError("Two mRNAs have the same ID: %s"%ID)
            feat = feature.CompoundDNASeqFeature(seqname=seqname,
                                                 display_id=display_id,
                                                 start=start,
                                                 length=length,
                                                 strand=strand,
                                                 key_vals=key_vals,
                                                 feat_type='mRNA')
            mRNAs[ID]=feat
    fh = utils.readfile(fname)
    # now get CDS and exons
    lineNumber = 0
    while 1:
        line = fh.readline()
        lineNumber = lineNumber + 1
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        if len(line)<9:
            # to avoid blank lines
            sys.stderr.write("Warning: weird line %i:%s\n"%(lineNumber,line))
            continue
        try:
            (seqname,producer,feat_type,start,end,
             ignore,strand,frame,extra_feat)=line.strip().split('\t')
        except ValueError:
            sys.stderr.write("Exception occurred at line %i:\n%s" % (lineNumber,line))
            raise
        if feat_type in ['exon','CDS']:
            start = int(start)-1
            end = int(end)
            length = end-start
            if strand == '+':
                strand = 1
            elif strand == '-':
                strand = -1
            else:
                raise ValueError("Strand makes no sense for: %s"%line)
            key_vals = parseKeyVals(extra_feat)
            parent_ID=key_vals['Parent']
            mRNA = mRNAs[parent_ID]
            feat = feature.DNASeqFeature(seqname=seqname,
                                         start=start,
                                         length=length,
                                         strand=strand,
                                         key_vals=key_vals,
                                         feat_type=feat_type)
            mRNA.addFeat(feat)
            if feat_type == 'CDS':
                if frame != '.':
                    try:
                        key_vals['transl_frame']=int(frame)
                    except ValueError:
                        sys.stderr.write("Warning: frame is not an int: %s"%line)
                        continue
    return mRNAs.values()

def parseKeyVals(extra_feat):
    "Parse extra feature fields."
    if extra_feat.endswith(';'):
        vals = extra_feat.split(';')[:-1]
    else:
        vals = extra_feat.split(';')
    key_vals = {}
    for item in vals:
        pair=item.split('=')
        if not len(pair)==2:
            raise ValueError("GFF line has weird extra feature value: %s."%extra_feat)
        key = pair[0]
        val = pair[1]
        if key_vals.has_key(key):
            raise ValueError("GFF line has two values for same key in extra feat field: %s."%extra_feat) 
        key_vals[pair[0]]=pair[1]
    return key_vals
            
        
