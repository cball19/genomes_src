#!/usr/bin/env python

ex=\
"""
Write HEADER.html text for QuickLoad genome version directories based on genome versions released at UCSC Genome Bioinformatics.
"""

import sys,fileinput,argparse

txt="""<html>
<body>
<h1>%s genome assembly</h1>
<p>
The files listed below are formatted for visualization in the Integrated Genome
Browser, available from <a href="http://www.bioviz.org/igb">BioViz.org</a>.
</p>
<p>
Annotation (bed) and alignment (psl) files were downloaded from the UCSC Genome
Bioinformatics Table Browser. Each file is named for its corresponding table in the
UCSC Genome database. See the <a href="annots.xml">annots.xml</a> meta-data file in
this directory for details.
</p>
<p>
Files with extension .gz were compressed and indexed using bgzip and
tabix tools from <a href="http://samtools.sourceforge.net">samtools.sourceforge.net</a>.
</p>
<p>
The file named %s.2bit contains sequence data and was downloaded from the UCSC Genome
Bioinformatics <a href="http://hgdownload-test.sdsc.edu/downloads.html">downloads
page</a>. To convert a .2bit to fasta, use twoBitToFa. The file
<a href="genome.txt">genome.txt</a> lists sequences and their sizes and was made from 
%s.2bit sequence file using twoBitInfo.
</p>
<p>
Both twoBitInfo and twoBitToFa are
available from <a href="http://hgdownload.cse.ucsc.edu/admin/exe/">http://hgdownload.cse.ucsc.edu/admin/exe/</a>.
</p>
</body>
</html>
"""

import os,fileinput,sys

def main(contents=None,directory=None):
    if directory.endswith(os.sep):
        directory=directory[0:-1]
    human_friendly_string = None
    for line in fileinput.input(contents):
        toks=line.rstrip().split('\t')
        if toks[0]==directory:
            human_friendly_string=toks[1]
            break
    if not human_friendly_string:
        raise ValueError("%s did not contain genome version directory %s"%
                         (contents,directory))
    sys.stdout.write(txt%(human_friendly_string,directory,directory))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=ex)
    parser.add_argument("directory",help='genome version')
    parser.add_argument('-c','--contents',help='contents file (default: contents.txt)',
                        dest='contents',default='contents.txt')
    args=parser.parse_args()
    main(directory=args.directory,
         contents=args.contents)
