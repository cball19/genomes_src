#!/usr/bin/env python

"""
Convert BED detail to gene_info.txt for Arabidopsis, rice, or some other format. 
Assumes gene model names are in the fourth field. 
"""

import fileinput, sys, optparse, re

def main(include_symbol=False,args=None):
    atreg=re.compile(r'AT[1-5CM]G\d{5}\.\d+')
    ricereg=re.compile(r'LOC_Os')
    d_descr={}
    d_sym={}
    for line in fileinput.input(args):
        toks=line.rstrip().split('\t')
        gene_id=toks[3]
        symbol=toks[12]
        descr = toks[13]
        if atreg.search(gene_id) or ricereg.search(gene_id):
            # it's rice or Arabidopsis
            gene_id = gene_id.split('.')[0]
        d_descr[gene_id]=descr
        d_sym[gene_id]=symbol
    if include_symbol:
        sys.stdout.write('id\tsymbol\tdescr\n')
    else:
        sys.stdout.write('id\tdescr\n')
    for gene_id in d_descr.keys():
        descr = d_descr[gene_id]
        symbol=d_sym[gene_id]
        if include_symbol:
            sys.stdout.write('\t'.join([gene_id,symbol,descr])+'\n')
        else:
            sys.stdout.write('\t'.join([gene_id,descr])+'\n')

if __name__ == '__main__':
    usage = '%prog [options] [FILE...]'
    parser=optparse.OptionParser(usage)
    parser.add_option('-s','--include_symbol',help="include symbol in output",
                      dest='include_symbol',default=False,action="store_true")
    (options,args)=parser.parse_args()
    main(include_symbol=options.include_symbol,args=args)
