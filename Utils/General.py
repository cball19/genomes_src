import re,gzip,xml.dom.minidom,cPickle,types,sys,zipfile,string,os

"""General utility functions for dealing with files and other common tasks."""

def write_obs(obs,suffix=None,obdir=None):
    """
    Function: Write the given objects to disk
    Returns : 
    Args    : obs - list of objects
              suffix - file name suffix to give the files
              obdir - directory to write the objects to
    """
    if not suffix or not obdir:
        raise Exception("suffix, obdir, or both not supplied\n")
    for ob in obs:
        write_ob(ob,suffix=suffix,obdir=obdir)

def write_ob(ob,suffix=None,obdir=None):
    """
    Function: Write the given object to disk as 'dump' file
    Returns : 
    Args    : ob - object to write, must implement display_id method
              suffix - file name suffix
              obdir - directory to save 'dump' file
    """
    if not suffix or not obdir:
        raise Exception("suffix, obdir, or both not supplied\n")
    fn = obdir + os.sep + ob.display_id() + suffix
    fh = open(fn,'w')
    cPickle.dump(ob,fh)
    fh.close()
    sys.stderr.write("Wrote: " + fn + "\n")
    return 1

def load_obs(obdir=None,suffix=None):
    """
    Function: Read objects saved in the given obdir directory into
              memory, as a list
    Returns:  a list of objects
    Args:     obdir - directory containing files with the given suffix
              suffix - optional, file name suffix required before reading
                       the object into memory
    """
    fs = os.listdir(obdir)
    objs = []
    for f in fs:
        if suffix and not f.endswith(suffix):
            continue
        fn = obdir + os.sep + f
        fh = open(fn,'r')
        ob = cPickle.load(fh)
        objs.append(ob)
        fh.close()
        sys.stderr.write("Loaded: " + fn + "\n")
    return objs

def identifiable2dict(lst):
    """
    Function: make a dictionary out of the given list of
              SeqFeature objects (or other objects with
              getDisplayId method)
    Returns : dictionary where display_id values are keys and
              feature.Identifiable objects are values
    Args    : lst - a list of SeqFeature objects or other type
                    of object with a getDisplayId method

    Note: if any items have the same display_id, only the last
          one in the list will be included in the dictionary.
    """
    d = {}
    for item in lst:
        k = item.display_id()
        d[k]=item
    return d

def write_doc(doc,fn):
    """
    Function: write the XML DOM to a file 
    Returns : 
    Args    : doc - object implementing XML document module
              fn - name of a file to create
    """
    f = open(fn,'w')
    f.write(doc.toprettyxml(indent='  ',newl='\n'))
    f.close()

def merge_dct(dcts):
    """
    Function: merge these dictionaries
    Returns : merged dictionary
    Args    : dcts - list of dictionary objects

    Note: if two dictionaries have the same key, only the key/value from
    the last dictionary will be kept.
    """
    newd = {}
    for d in dcts:
        for key in d.keys():
            newd[key]=d[key]
    return newd

def num_cmp(x,y,ascend=1):
    """
    Function: Compares two floats
    Returns : -1 if y is bigger, 1 if x is bigger
    Args    : x,y - two floats
              ascend - 1 if sort in ascending order
                       0 if sort in descending order

    Pass this to a list's 'sort' method.
    """
    if is_string(x):
        x = string.atof(x)
    if is_string(y):
        y = string.atof(y)
    diff = y - x
    if diff > 0:
        return -1
    if diff < 0:
        return 1
    if diff == 0:
        return 0
    
def run(cursor=None,sql=None):
    """
    Function: Run the given sql statement using the given cursor
    Returns : Tuples of results.
    Args    : cursor - a cursor with read access
              sql - an sql statement to execute
    """
    cursor.execute(sql)
    return cursor.fetchall()

def measure_fields(d):
    """
    Function:  compute the maximum length of each field in the given
                  data dictionary
    Returns : dictionary in which field names are keys and maximum length of
              the field is the value
    Args    : name of a data file
    """
    poster_child = d.values()[0]
    if is_list(poster_child):
        poster_child = poster_child[0]
    toks = poster_child.keys()
    toks_d = {}
    for tok in toks: 

        toks_d[tok]=0
    subds = d.values()
    for subd in subds:
        if is_list(subd):
            for sub in subd:
                _measure_fields(sub,toks,toks_d)
        else:
            _measure_fields(subd,toks,toks_d)
    return toks_d

def _measure_fields(subd,toks,toks_d):
    """
    Function:
    Returns:
    Arguments:
    """
    for tok in toks:
        try:
            val = subd[tok]
            length = len(val)
            if length > toks_d[tok]:
                toks_d[tok] = length
        except KeyError:
            sys.stderr.write("Warning: no value for field name: " + \
                             tok + ", key: " + key + '\n') 

def is_int(x):
    """
    Function: find out if the given item is a integer data type or not
    Returns : 1 if yes, 0 if not
    Args    : anything, hopefully an int

    Syntactic sugar for interrogating __class__ field
    """
    if x.__class__ is types.IntType:
        return 1
    else:
        return 0

def is_string(x):
    """
    Function: find out if the given item is a string data type or not
    Returns : 1 if yes, 0 if not
    Args    : anything

    Syntactic sugar for interrogating __class__ field
    """
    if x.__class__ is types.StringType:
        return 1
    else:
        return 0

def is_list(x):
    """
    Function: find out if the given item is a list data type or not
    Returns : 1 if yes, 0 if not
    Args    : something - a list, whatever

    Syntactic sugar for interrogating __class__ field
    """
    if x.__class__ is types.ListType:
        return 1
    else:
        return 0

def is_dict(x):
    """
    Function: find out if the given item is a dictionary data type or not
    Returns : 1 if yes, 0 if not
    Args    : something - a list, whatever

    Syntactic sugar for interrogating __class__ field
    """
    if x.__class__ is types.DictType:
        return 1
    else:
        return 0

def write_keys(dct,fh):
    """
    Function: write the keys to the given file handle, one key
              per line
    Returns : number of keys written
    Args    : a dictionary
              a filehandle

    Note: keys must be strings

    Later, add type checking for numeric keys.
    """
    key_iter = 0
    for key in dct.keys():
        fh.write(key + '\n')
        key_iter = key_iter + 1
    return key_iter

def dump_data(structure,fn):
    """
    Function: use the cPickle module to save a data structure to disk
    Returns : nothing
    Args    : a data structure and the name of a file
    """
    f = open(fn,'w')
    pkl = cPickle.Pickler(f,1)
    pkl.dump(structure)
    f.close()

def init_doc():
    """
    Function: Create and configure an xml.dom.minidom.Document object
    Returns : an xml.dom.minidom.Document object and its top node
    Args    : the title
    """
    doc = xml.dom.minidom.Document()
    topnode = doc.createElement("html")
    doc.appendChild(topnode)
    body = doc.createElement("body")
    body.setAttribute("bgcolor","ffffff")
    topnode.appendChild(body)
    return (doc,body)

def readfile(f):
    """
    Function: Open a file for reading.
    Returns : a file handle
    Args    : the name of a file

    The given file can be compressed (file extension .gz)
    or a regular non-compressed file.
    """
    if f.endswith('.gz'):
        z = gzip.GzipFile(f)
    else:
        z = open(f)
    return z

def missing_keys(d1,d2):
    """
    Function: find out what keys are d1 that are NOT in d2
    Returns : a list of the missing keys
    Args    : d1, d2 - two dictionaries
    """
    missing = []
    for key in d1.keys():
        if not d2.has_key(key):
            missing.append(key)
    return missing

def purge_missing_keys(d1,d2):
    """
    Function: return a copy of d2 that contains only keys that are also
              in d1
    Returns : 
    Args    : d1, d2 - two dictionaries
    """
    nd = {}
    for key in d1.keys():
        if d2.has_key(key):
            nd[key] = d2[key]
    return nd

def tab2toks(f):
    """
    Function: Read a tab-delimited file
    Returns : list of lists
    Args    : the name of the file

    """
    return delim2toks(f,'\t')

def delim2toks(f,c):
    """
    Function: Read a delimited file
    Returns : list of lists
    Args    : the name of the file, character delimiter
    """
    fh = open(f,'r')
    tokslist = []
    while 1:
        line = fh.readline()
        if not line:
            break
        line = line.rstrip()
        toks = line.split(c)
        tokslist.append(toks)
    return tokslist

def delim2dict(lines,c,keyfield,fnames=None):
    """
    Function: Read a delimited tabular file, convert it to a dictionary, where
              keys are values from the keyfield column 
    Returns : a dictionary, where values are from the keyfield column and
              and values are a dictionary of columns, keyed by column
              headings
    Args    : lines - list of lines from a delimited file
              c - file column delimiter (e.g., \t for tab delimited)

    Note: if a column heading or value is wrapped in double-quotes, removes
    these.

    Expects that input lines do not have newline characters.
    """
    dct = {}
    if fnames:
        cols = fnames
    else:
        cols = lines[0].split(c)
        ncols = []
        for col in cols:
            ncol = col
            if col[0]=='"':
                ncol = col[1:]
            if col[-1] == '"':
                ncol = ncol[:-1]
            ncols.append(ncol)
            cols = ncols
    nline = 0
    for line in lines[1:]:
        nline = nline + 1
        toks = line.split(c)
        if len(toks) != len(cols):
            sys.stderr.write("Warning: wrong number fields on line: " + \
                             repr(nline) + '\n')
            sys.stderr.write("Skipping it.\n")
            continue
        i = 0
        subd = {}
        for colhead in cols:
            tok = toks[i]
            if len(tok)> 0:
                if tok[0]=='"':
                    tok = tok[1:]
                if tok[-1]=='"':
                    tok = tok[:-1]
            subd[colhead]=tok
            if i == keyfield:
                if dct.has_key(tok):
                    prev = dct[tok]
                    if is_list(prev):
                        prev.append(subd)
                    else:
                        dct[tok]=[dct[tok],subd]
                else:
                    dct[tok]=subd
            i = i + 1
    sys.stderr.write("Read: " + repr(nline) + " lines.\n")
    return dct

def get_fields(dct):
    """
    Function: returns a dictionary of subdictionary field names from the
              dct
    Returns : a dictionary, keys are the sub-dictionary keys and values
              are a count of how many sub-dictionaries contained that sub-key
    Args    : dct, a double-dictionary

    """
    keys = dct.keys()
    fields = {}
    for key in keys:
        subdct = dct[key]
        subkeys = subdct.keys()
        for subkey in subkeys:
            if fields.has_key(subkey):
                fields[subkey] = fields[subkey] + 1
            else:
                fields[subkey] = 1
    return fields


def mkdir(d,verbose=False):
    """
    Function: Make directory if it doesn't already exists.
              Print a message to stderr if verbose is True.
    Args    : string, name of the directory 
    Returns : 
    """
    if verbose:
        sys.stderr.write("About to create directory %s.\n"%d)
    try:
        os.mkdir(d)
    except OSError:
        if verbose:
            sys.stderr.write("No need to create directory %s. It already exists.\n"%d)

def fileExists(fn):
    "Find out if a file exists by attempting to open it."
    # http://stackoverflow.com/questions/82831/how-do-i-check-if-a-file-exists-using-python
    try:
        fh = open(fn,'rb')
        fh.close()
        return True
    except IOError:
        return False

def checkDir(d):
    """
    Function: Check whether the given directory exists and is readable.
    Args    : d - string, name of the directory 
    Returns : True or False
    """
    try:
        os.listdir(d)
        return True
    except OSError:
        return False

def runCommand(cmd,verbose=False):
    "Run the given cmd, a command. Mainly for Unix."
    cwd = os.getcwd()
    if verbose:
        sys.stderr.write("running: %s in %s\n"%(cmd,cwd))
    return os.popen(cmd)

