#!/usr/bin/env python

"Read a CuffLinks GTF file and write out a BED version"

import os,sys,re,optparse
import Mapping.Parser.Bed as Bed
import Mapping.Parser.CufflinksGtf as p
import Mapping.FeatureModel

def readGtfFile(fname=None):
    models = p.gff2feats(fname)
    return models

def writeBedFile(fname=None,feats=None):
    fh=open(fname,'w')
    writeBed(fh=fh,feats=feats)
    fh.close()

def writeBed(fh=None,feats=None):
    for feat in feats:
        bedline = Bed.feat2bed(feat).rstrip()
        gene = feat.getVal('gene_id')
        keys=filter(lambda x:x not in ['transcript_id','gene_id','exon_number'],
                    feat.getKeyVals().keys())
        FPKM=feat.getVal('FPKM')
        #toks = map(lambda x:"%s=%s"%(x,feat.getVal(x)),keys)
        bedline=bedline+'\t'+gene+'\t'+FPKM+'\n'
        if fh:
            fh.write(bedline)
        else:
            sys.stdout.write(bedline)
    if fh:
        fh.close()

def convert(bed_file=None,
            gtf_file=None):
    feats = readGtfFile(fname=gtf_file)
    writeBedFile(fname=bed_file,feats=feats)

def main(bed_file=None,gtf_file=None):
    convert(bed_file=bed_file,
            gtf_file=gtf_file)

if __name__ == '__main__':
    usage = "%prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-g","--gtf_file",help="Cufflinks GTF file to convert",dest="gtf_file"),
    parser.add_option("-b","--bed_file",help="BED format file to write",
                      dest="bed_file",default=None)
    (options,args)=parser.parse_args()
    if not options.bed_file or not options.gtf_file:
        parser.error("Bed or GTF files not specified.")
    main(gtf_file=options.gtf_file,
         bed_file=options.bed_file)

