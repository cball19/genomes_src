# Python code for IGBQuickLoad genomes data repository #

This repository contains (mostly) python code for managing IGB QuickLoad repositories of genomic data. 

### Setting up ###

To use this code:

* Clone the repository 
* Add the directory **genomes_src** to your PYTHONPATH and PATH

### Tests ###

To run tests, enter **Tests** directory and run **run_tests.py**.

### Contribution guidelines ###

If you would like to contribute, please fork the repository in bitbucket, make changes there, and then issue a pull request.

### Questions? Comments? ###

Please contact

* Ann Loraine (aloraine@uncc.edu)
