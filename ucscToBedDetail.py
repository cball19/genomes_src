#!/usr/bin/env python

"""
A script that converts BED format to BED detail using:

BED file from UCSC Genome Bioinformatics Table Browser

and

ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene2accession.gz

and

ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz

"""

import Utils.General as utils
import os,sys,optparse

def main(bed_file=None,
         out_file=None,
         gene2accession='gene2accession.gz'):
    """
    Function: add gene name and gene description fields (13, 14)
              to a BED 12 file
    Returns : 
    Args    : bed_file - name of BED file to convert
              out_file - name of file to write

    If bed_file is None, read from sys.stdin.
    If out_file is None, write to sys.stdout.
    """ 
    bed = readBed(fn=bed_file)
    gene_info=readGeneInfo()
    acc2gene_id = readGene2Accession(bed=bed,fn=gene2accession)
    mergeBed(bed=bed,gene_info=gene_info,outfile=out_file,acc2gene=acc2gene_id)


def readBed(fn=None):
    """
    Function: read a BED12 format file
    Returns : dictionary, keys are name (field 4) from BED
              values are lists of lists
              one mRNA may have multiple mappings and we store each one
    Args    : fn - name of BED format file
    """ 
    if not fn:
        fh = sys.stdin
    else:
        fh = utils.readfile(fn)
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            if fn:
                fh.close()
            break
        toks = line.rstrip().split('\t')
        name = toks[3]
        if d.has_key(name):
            d[name].append(toks)
        else:
            d[name]=[toks]
    return d

def readGene2Accession(fn='gene2accession.gz',
                      bed=None):
    """
    Function: get mapping of accessions (keys in bed) to
              unique gene ids from NCBI 
    Returns : dictionary, keys are accessions (from bed)
              values are gene ids
              Note: only keep one gene id. Some mRNAs may
              map to multiple genes. We'll overlook these
              pathological cases.
    Args    : fn - name of gene2accession file from NCBI
              bed - output of readBed [required]

    Tip: To make this go faster, pre-filter by taxonomy id, the first
         field in the gene2accession file.
    """
    fh = utils.readfile(fn)
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        toks = line.rstrip().split('\t')
        try:
            gene_id = toks[1]
        except IndexError:
            sys.stderr.write("Warning: Can't read gene id from %s"%line)
            continue
        try:
            accession = toks[3].split('.')[0]
        except IndexError:
            sys.stderr.write("Warning: Can't read accession from %s\n"%toks[3])
        if bed.has_key(accession):
            d[accession]=gene_id
    return d

def readGeneInfo(fn='gene_info.gz'):
    "Read all gene and return a dictionary where key are gene ids and values are lists containing symbol and descritiption"
    fh = utils.readfile(fn)
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        toks = line.rstrip().split('\t')
        gene_id = toks[1]
        gene_symbol = toks[2]
        gene_description=toks[8]
        d[gene_id]=[gene_symbol,gene_description]
    return d

def mergeBed(bed=None,gene_info=None,outfile=None,
             acc2gene=None):
    if not outfile:
        outfh = sys.stdout
    else:
        outfh = open(outfile,'w')
    for name in bed.keys():
        tokss = bed[name]
        for toks in tokss:
            if acc2gene.has_key(name):
                gene_id = acc2gene[name]
                symbol=gene_info[gene_id][0]
                description=gene_info[gene_id][1]
            else:
                sys.stderr.write("No gene id for %s.\n"%name)
                symbol='NA'
                description='NA'
            newtoks = list(toks)
            newtoks.append(symbol)
            newtoks.append(description)
            line = '\t'.join(newtoks)+'\n'
            outfh.write(line)
    if outfile:
        outfh.close()

if __name__ == '__main__':
    usage = "%prog [options] [bedfile] [outfile]"
    parser = optparse.OptionParser(usage)
    parser.add_option('-a','--accession_file',dest='gene2accession',
                      type='string',default='gene2accession.gz',
                      help='File mapping accessions to gene ids [default is gene2accession.gz]')
    (options,args)=parser.parse_args()
    if len(args) == 2:
        bed_file = sys.argv[1]
        out_file = sys.argv[2]
    elif len(args) == 1:
        bed_file = sys.argv[1]
        out_file = None
    elif len(args) == 0:
        bed_file = None
        out_file = None
    if bed_file =='-':
        bed_file = None
    main(bed_file=bed_file,
         out_file=out_file,
         gene2accession=options.gene2accession)
