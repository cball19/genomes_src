#!/usr/bin/env python

"""Retrieve and format the TIGRv5 chromosome sequences."""

import sys,os,re

"""
From TAIR Curator Chris Wilks:

Chris Wilks (JIRA) <curator@arabidopsis.org> 	Tue, Dec 22, 2009 at 8:51 PM
To: aloraine@gmail.com

Hi Ann,

The assembly of the chromosome sequence has changed twice since TIGRv5.  In TAIR8 we made only substitutions, so the coordinates of genes and other mapped objects are the same between TIGRv5 and TAIR6, TAIR7, and TAIR8 (for those genes whose annotation didn't change for each release).

In TAIR9 we included INDELs in our updates, so the coordinates are different between all the releases mentioned in the previous sentence and TAIR9.

But what you want is the original TIGRv5 chromosomes which served as the basis for TIGRv5,TAIR6, and TAIR7.

You were right to think that the current chr1-5 and M+C files in OLD are a later version (TAIR8), although their lengths would have been the same as TIGRv5.
However, the ones you are looking for are the following (also in that directory):

ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr1.1con.01222004
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr2.1con.01222004
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr3.1con.01222004
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr4.1con.01222004
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr5.1con.01222004

and

ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/mitochondrial_genomic_sequence
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chloroplast.1con.01072002

Let us know if you have any more questions,
Chris Wilks
TAIR

URLs beginning with http://germany in this email are for use by TAIR staff only, please disregard them.

> question regarding TIGRv5 chromosome sequence files
> ---------------------------------------------------
>
>                 Key: HLP-4513
>                 URL: http://germany.tairgroup.org:8090/jira/browse/HLP-4513
>             Project: HelpDesk
>          Issue Type: Service Request
>            Reporter: Ann Loraine
>            Assignee: Chris Wilks
>
> Dear Curators,
> I am working on validating some new microarray probe mapping files for some
> data sets that were generated using arrays designed against the old TIGRv5
> genome assembly.
> To proceed, I need to obtain some "fasta" files for the TIGRv5
> pseudochromosomes.
> Could you help me retrieve these from your ftp site or other location?
> I found this directory on the TAIR ftp site but am not sure if these refer
> to the TIGRv5 version or something later:
> ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/
> Sincerely,
> Ann Loraine


"""
    
ftps = """ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr1.1con.01222004
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr2.1con.01222004
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr3.1con.01222004
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr4.1con.01222004
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chr5.1con.01222004
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/mitochondrial_genomic_sequence
ftp://ftp.arabidopsis.org/home/tair/Sequences/whole_chromosomes/OLD/ATH1_chloroplast.1con.01072002"""

def grabSeqs():
    """
    Function: Download from TAIR the TIGRv5 sequences.
    Args    : 
    Returns :
    """
    lst = ftps.split('\n')
    for f in lst:
        localname = f.split('/')[-1]
        try:
            stat_info = os.stat(localname)
            sys.stderr.write("No need to get: " + f + "\n")
            sys.stderr.write("It's here with size: " + \
                             str(st_size) + " bytes.\n")
        except OSError:
            cmd = 'curl -O ' +  f
            sys.stderr.write("Executing:\n"+cmd+'\n')
            os.system(cmd)
            sys.stderr.write("Retrieved it.")
        

def getNamesMapping():
    """
    Function: Maps file names to chromosome names
    Args    : 
    Returns : dictionary of files to chromosome names
    """
    reg = re.compile(r'chr[1-5]')
    lst = ftps.split('\n')
    lst = map(lambda x:x.split('/')[-1],lst)
    d = {}
    for f in lst:
        m = reg.search(f)
        if m:
            d[f]=m.group()
        elif f.startswith('mitochondrial'):
            d[f]='chrM'
        elif f.startswith('ATH1_chloroplast'):
            d[f]='chrC'
    return d
        

