"""Add gene name and description to tomato BED file."""

import re,sys,os

namereg=re.compile(r'Name=(.*)')
def getNameToDescr(fn='ITAG2.3_gene_models.gff3'):
    fh = open(fn)
    d = {}
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        if line.startswith('#'):
            continue
        toks = line.rstrip().split('\t')
        if toks[2]=='mRNA' and 'Note' in toks[-1]:
            toks=toks[-1].split(';')
            name = None
            description = None
            for tok in toks:
                if tok.startswith('Name='):
                    name=tok.split('Name=')[1]
                elif tok.startswith('Note='):
                    description=tok.split('Note=')[1]
            if name and description:
                d[name]=description
    return d

def makeBedDetail(fn='S_lycopersicum_May_2012.bed',
                  d=None,
                  outfn='test.bed'):
    if not d:
        d = getNameToDescr()
    infh = open(fn)
    outfh = open(outfn,'w')
    while 1:
        line = infh.readline()
        if not line:
            infh.close()
            break
        toks = line.rstrip().split('\t')
        name = toks[3]
        gene_name_toks = name.split('.')
        gene_name = '.'.join(gene_name_toks[0:-1])
        if d.has_key(name):
            description = d[name]
        else:
            description = 'NA'
        toks.append(gene_name)
        toks.append(description)
        newline = '\t'.join(toks)+'\n'
        outfh.write(newline)
    outfh.close()


