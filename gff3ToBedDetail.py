#!/usr/bin/env python

"Read GFF3 file and write out as BED14 (BED detail) format."

import os,sys,re,optparse
import Mapping.Parser.Bed as Bed
import Mapping.Parser.Gff3 as p

def readGffFile(fname=None):
    models = p.gff2feats(fname)
    producers={}
    for model in models:
        gene_name=model.getVal('Parent')
        key_vals=model.getKeyVals()
        if key_vals.has_key('Name'):
            del key_vals['Name'] # redundant with display id
        del key_vals['ID'] # redundant with display id
        del key_vals['Parent'] # redundant with gene_name
        if key_vals.has_key('producer'):
            producer=key_vals['producer']
            producers[producer]=1
        if gene_name.startswith('gene:'):
            gene_name=gene_name[5:]
        # Sol Genomics Network prepends "gene:" and "mRNA:" onto
        # names
        display_id=model.getDisplayId()
        if display_id.startswith('mRNA:'):
            model.setDisplayId(display_id[5:])
        model.setKeyVal('gene_name',gene_name)
    if len(producers.keys())==1:
        for model in models:
            key_vals = model.getKeyVals()
            if key_vals.has_key('producer'):
                del key_vals['producer']
    # SolCyc prepends ids with mRNA and gene
    # fix this
    return models

def writeBedFile(fname=None,feats=None):
    Bed.feats2bed(feats,fname=fname,bed_format='BED14')

def convert(bed_file=None,
            gff_file=None):
    feats = readGffFile(fname=gff_file)
    writeBedFile(fname=bed_file,feats=feats)

def main(bed_file=None,gff_file=None):
    convert(bed_file=bed_file,
            gff_file=gff_file)

if __name__ == '__main__':
    usage = "%prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-g","--gff_file",help="GFF3 file to convert. Can be compressed. [required]",dest="gff_file"),
    parser.add_option("-b","--bed_file",help="BED14 (bed detail) format file to write [required]",
                      dest="bed_file",default=None)
    (options,args)=parser.parse_args()
    if not options.bed_file or not options.gff_file:
        parser.error("Bed or GFF files not specified.")
    main(gff_file=options.gff_file,
         bed_file=options.bed_file)
