#!/usr/bin/env python

import optparse,sys,fileinput,re,os

ex=\
"""
Given a fastq file containing read1 and read2 from the same sequencing
run and a prefix, write all read1's to one file and all read2's to
another file.

Files will be named PREFIX_1.fastq and PREFIX_2.fastq.

Files will only be written if the original file contained reads1 or
reads2.

Accepts input on stdin or multiple arguments.

Writes to stdout.
"""

def main(args,prefix=None):
    fh1=None
    fh2=None
    fh = None
    counter=0
    for line in fileinput.input(args):
        if counter%4==0:
            read=line.split()[1].split(':')[0]
            if read=='1':
                if not fh1:
                    fh1=open(getFnameR1(prefix),'w')
                fh=fh1
            elif read=='2':
                if not fh2:
                    fh2=open(getFnameR2(prefix),'w')
                fh=fh2
            else:
                cleanup(prefix) # delete possibly broken files
                raise ValueError("Wrong read name at %i: %s"%(line,counter+1))
        fh.write(line)
        counter=counter+1
    if not counter%4==0:
        cleanup(prefix)
        raise ValueError("Wrong number of reads in input.")
    return counter

def getFnameR1(prefix):
    return prefix+'_1.fastq'

def getFnameR2(prefix):
    return prefix+'_2.fastq'

def cleanup(prefix):
    r1=getFnameR1(prefix)
    r2=getFnameR2(prefix)
    if os.path.exists(r1):
        os.remove(r1)
    if os.path.exists(r2):
        os.remove(r2)

if __name__ == '__main__':
    usage = "%prog"+ex
    parser = optparse.OptionParser(usage)
    parser.add_option("-p","--prefix",help="Base name for read1 and read2 files [required]",dest="prefix")
    (options,args)=parser.parse_args()
    if not options.prefix:
        parser.error("Base name for read1 and read2 files not specified.")
    main(args,prefix=options.prefix)
