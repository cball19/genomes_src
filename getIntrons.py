#!/usr/bin/env python

ex=\
"""
Read a BED file and extract intron features. 
Reads from stdin or one or more files.
Writes introns feature in BED14 to stdout.
Intron features are named for their genomics coordiantes.

example)

  gunzip -c TAIR10.bed.gz | %prog > out.tsv
"""

import sys,fileinput,optparse,re


tab='\t'
nl='\n'
reg=re.compile(r'\.\d+$')

def main(args):
    for line in fileinput.input(args):
        # ignore track line headers that are sometimes at the start
        # of a file
        if line.startswith('track'):
            continue
        newlines = makeIntronLines(line)
        txt = nl.join(newlines)
        sys.stdout.write(txt)

def makeIntronLines(line):
    (chrom,chromStart,name,score,strand,itemRGB,blockSizes,
     blockStarts)=parseBedLine(line)
    i = 0
    newlines=[]
    chromStart=int(chromStart)
    if reg.search(name):
        toks=gene_name.split('.')
        gene_name='.'.join(toks[0:-1])
    else:
        gene_name=name
    for blockStart in blockStarts[:-1]:
        blockSize=blockSizes[i]
        intron_start=blockStart+blockSize+chromStart
        intron_end=blockStarts[i+1]+chromStart
        intron_name = makeIntronName(start=intron_start,
                                     end=intron_start,
                                     seqname=chrom)
        newline = tab.join([chrom,str(intron_start),
                            str(intron_end),intron_name,score,
                            strand,str(intron_start),
                            str(intron_start),itemRGB,
                            '1',str(intron_end-intron_start)+',',
                            str(intron_start)+',',gene_name,'NA'])+'\n'
        newlines.append(newline)
        i = i + 1
    return newlines
            

def parseBedLine(line):
    toks = line.rstrip().split('\t')
    # see: http://genome.ucsc.edu/FAQ/FAQformat.html#format1
    chrom=toks[0]
    chromStart = int(toks[1])
    name=toks[3]
    score=toks[4]
    strand=toks[5]
    itemRGB=toks[8]
    # UCSC beds put , at end of fields
    # TopHat doesn't
    if toks[10].endswith(','):
        toks[10]=toks[10][0:-1]
    blockSizes=map(lambda x:int(x),toks[10].split(','))
    if toks[11].endswith(','):
        toks[11]=toks[11][0:-1]
    blockStarts=map(lambda x:int(x),toks[11].split(','))        
    return (chrom,chromStart,name,score,strand,itemRGB,blockSizes,
            blockStarts)

def makeIntronName(start=None,
                   end=None,
                   seqname=None):
    """
    Make a name for the intron that indicates its coordinates on the genome and the gene model it came from.
    """
    name='%s:%i-%i'%(seqname,start,end)
    return name

if __name__ == '__main__':
    usage='%prog [ file ... ]\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)
