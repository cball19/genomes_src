#!/usr/bin/env python

import argparse

ex=\
"""
Read gene models BED-detail or BED12 file and output largest transcript size per
gene. Name field must use N.N suffix scheme to designate alternative
gene models arising from the same gene, where N is a numeral. 

For example, AT1G07350.1 and AT1G07350.2 are two alternative transcripts 
transcribed from locus AT1G07350. 

Similarly, CUFF.1.1 and CUFF.1.2 are alternative transcripts arising from
locus CUFF.1.

Transcript identifiers that do not satisfy the pattern are assumed to
be the only transcript arising from the gene. For example, ChrSy.fgenesh.mRNA.1
is assumed to be the only transcript arising from gene ChrSy.fgenesh.mRNA.1.

Lastly, if field 13 and field 4 contain the same value, assumes both
are the locus identifier.

Accepts input on stdin and writes to stdout.
"""

import sys,fileinput,re

def main():
    reg=re.compile(r'[0-9]$')
    N = 0
    d={}
    lst=[]
    for line in fileinput.input():
        if not line.startswith('track') or not line.startswith('#'):
            toks=line.split('\t')
            transcript_name=toks[3]
            field13=None
            locus_id=None
            # this is so embarassing
            if len(toks)==14: # it's bed detail
                field13=toks[12]
                if field13==transcript_name:
                    locus_id=field13 # locus and transcript id are the same, no alt transcripts allowed
                else:
                    name_parts=transcript_name.split('.')
                    # allow AT1G07340.1, CUFF.1.1 but not ChrSy.fgenesh.mRNA.11
                    if len(name_parts)>1 and reg.search(name_parts[-2]):
                        locus_id='.'.join(name_parts[0:-1])
                    else:
                        locus_id=field13
            else:
                name_parts=transcript_name.split('.')
                # allow AT1G07340.1, CUFF.1.1 but not ChrSy.fgenesh.mRNA.11
                if len(name_parts)>1 and reg.search(name_parts[-2]):
                    locus_id='.'.join(name_parts[0:-1])
                else:
                    locus_id=transcript_name
            blocksizes=toks[10]
            if blocksizes.endswith(','):
                blocksizes=blocksizes[:-1]
            txsize=sum(map(lambda x:int(x),blocksizes.split(',')))
            if not d.has_key(locus_id):
                lst.append(locus_id)
                d[locus_id]=txsize
            else:
                if d[locus_id]<txsize:
                    d[locus_id]=txsize
    fh=sys.stdout
    fh.write("locus\tbp\n")
    for locus in lst:
        txsize=d[locus]
        fh.write('%s\t%i\n'%(locus,txsize))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=ex)
    args=parser.parse_args()
    main()
