#!/usr/bin/env python

import unittest
import os,sys

import splitPairs as s

class SplitPairs(unittest.TestCase):

    R1='data/fastq/R1.fastq'
    R2='data/fastq/R2.fastq'
    wrongLines='data/fastq/wrongLines.fastq'
    R1R2='data/fastq/R1R2.fastq'
    
    def testDontWriteWrongFile(self):
        "If given a file with only R1, only one file should be written."
        prefix='testdontWriteWrongFile'
        count=s.main(self.R1,prefix=prefix)
        r1=s.getFnameR1(prefix)
        r2=s.getFnameR2(prefix)
        was_there=os.path.exists(r2)
        if was_there:
            os.remove(r2)
        os.remove(r1)
        right_answer=False
        self.assertEquals(right_answer,was_there)
        
    def testCountFastq(self):
        "main should return number of lines read"
        prefix='testCountFastq'
        count=s.main(self.R1,prefix=prefix)
        fname=s.getFnameR1(prefix)
        os.remove(fname)
        right_answer=400
        self.assertEquals(right_answer,count)

    def rightFiles(self):
        "main should write two files, one for read1 and another for read2"
        prefix='rightFiles'
        r1=s.getFnameR1(prefix)
        r2=s.getFnameR2(prefix)
        count=s.main(self.R1R2,self=prefix)
        right_answer=True
        result=os.path.exists(r1) and os.path.exists(r2)
        s.cleanup(prefix)
        self.assertEquals(right_answer,result)

    def testWrongNumLines(self):
        "main should generate ValueError if fastq file has wrong number of lines."
        prefix='testWrongNumLines'
        fname=s.getFnameR1(prefix)
        self.assertRaises(ValueError,s.main,self.wrongLines,prefix)

if __name__ == "__main__":
    unittest.main()


    
